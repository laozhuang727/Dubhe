/**
 * Copyright 2020 Tianshu AI Platform. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================
 */

package org.dubhe.constant;

/**
 * @description 数字常量
 * @date 2020-06-09
 */
public class NumberConstant {

    public final static int NUMBER_0 = 0;
    public final static int NUMBER_1 = 1;
    public final static int NUMBER_2 = 2;
    public final static int NUMBER_3 = 3;
    public final static int NUMBER_4 = 4;
    public final static int NUMBER_5 = 5;
    public final static int NUMBER_6 = 6;
    public final static int NUMBER_8 = 8;
    public final static int NUMBER_10 = 10;
    public final static int NUMBER_30 = 30;
    public final static int NUMBER_50 = 50;
    public final static int NUMBER_60 = 60;
    public final static int NUMBER_1024 = 1024;
    public final static int NUMBER_900 = 900;
    public final static int NUMBER_100 = 100;

}
